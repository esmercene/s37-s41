const mongoose = require('mongoose')

const user_schema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, 'First Name name is Required']
	},
	lastName: {
		type: String,
		required: [true, 'Last Name is Required']
	},
	email: {
		type: String,
		required: [true, 'Email is Required']
	},
	password: {
		type: String,
		required: [true, 'Password is Required']
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: String,
		required: [true, 'Mobile Number is Required']
	},
	enrollments: [
		{
			courseId: {
					type: String,
				required: [true, 'Course ID is Required']
		    },
		    enrolledOn: {
				    type: Date,
				  default: new Date()
			},
			status: {
					type: String,
				 default: "Enrolled"
			}
			
		}
	]
})

module.exports = mongoose.model('User', user_schema)