const mongoose = require('mongoose')

const course_schema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, 'Course name is Required.']
	},
	description: {
		type: String,
		required: [true, 'Description is Required']
	},
	price: {
		type: Number,
		required: [true, 'Price is Required']
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	enrollees: [
		{
			 userId: {
				type:String,
			    required: [true, 'userID is Required']
				}
		},
		{   
			 enrolledOn: {
				type: Date,
				default: new Date()
			    }
		}
	]
})

module.exports = mongoose.model('Course', course_schema)