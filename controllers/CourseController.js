const Course = require('../models/Course')



// module to add a course
module.exports.addCourse = (data) => {
	if(data.isAdmin){
		let new_course = new Course({
		name: data.course.name,
		description: data.course.description,
		price: data.course.price		
		
	 })

	 return new_course.save().then((new_course, error) => {
		if(error){
			return false
		 }
		 return { message: "New Course Successfully Created!"}
	  })
	}
		let message = Promise.resolve({message: "User must be ADMIN to access this"})
		return message.then((value) => {
			return value
    })
}


// module to get all courses
module.exports.getAllCourses = () => {
	return Course.find({}).then((result) =>{
		return result
	})
}


// module to get all active courses
module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then((result) =>{
		return result
	})
}

// module to get single course
module.exports.getCourse = (course_id) => {
	return Course.findById(course_id).then((result) =>{
		return result
	})
}


// module to update a course
module.exports.updateCourse = (course_id, new_data) => {
	return Course.findByIdAndUpdate(course_id, {
		name: new_data.name,
		description: new_data.description,
		price: new_data.price
	}).then((updated_course, error) => {
		if(error) {
			return false
		}

		return { message: "Course has been update successfully!"}
	})
}

// module to archive a course
module.exports.archiveCourse = (course_id, archive) => {
	return Course.findByIdAndUpdate(course_id, {isActive: false}).then((archive, error) => {
		if(error) {
			return false
		}

		return { message: "Course has been archived successfully!"}
	})
}





