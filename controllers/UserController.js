const User = require('../models/User')
const Course = require('../models/Course')
const bcrypt = require('bcrypt')
const auth = require('../auth')



// module to check if email exist
module.exports.checkIfEmailExists = (data) => {
	return User.find({email: data.email}).then((result) => {
		if(result.length > 0){
			return true
		}
		return false
	})
}

// module to register new user 
module.exports.register = (data) => {
	let encrypted_password = bcrypt.hashSync(data.password, 10)

	let new_user = new User({
		firstName: data.firstName,
		lastName: data.lastName,
		email: data.email,
		mobileNo:  data.mobileNo,
		password:  encrypted_password

	})

	return new_user.save().then((created_user, error) => {
		if(error){
			return false
		}

		return {
			message: 'User Successfully Registered!'
		}
	})
	
}


// module to login user 
module.exports.login = (data) => {
	return User.findOne({email: data.email}).then((result) => {
		if (result == null){
			return {message: "User Doesn't Exist"}
		}

		const is_password_correct = bcrypt.compareSync(data.password, result.password)
		if(is_password_correct) {
			return { accessToken: auth.createAccessToken(result)}
		}

		return { message: "Invalid Password!!!"}
	})
}


// module to get single user 
module.exports.getUserDetails = (user_id) => {
	return User.findById(user_id, {password: 0}).then((result, error) => {
		return result
	})
}


// module to enroll to a course
module.exports.enroll = async(data) => {
	// Check if user is done adding the course to its enrollments array 
	let is_user_updated = await User.findById(data.userId).then((user) => {
		user.enrollments.push({
			courseId: data.courseId
		})

		return user.save().then((updated_user, error) => {
			if(error){
				return false
			}

			return true
		})
	})

	// Check is course is done addind the user to its enrollees 
	let is_course_updated =  await Course.findById(data.courseId).then((course) => {
		course.enrollees.push({
			userId: data.userId
		})

		return course.save().then((updated_course, error) => {
			if(error){
				return false
			}

			return true
		})
	})

	// Check if both the user and course have been updated successfully and return a success meassage if so 
	if(is_user_updated && is_course_updated){
		return { message: "User Enrollment is Successful!"}
	}

	// If the enrollment failed, return a message "Something went wrong"
	return { message: "Something Went Wrong!"}
}







